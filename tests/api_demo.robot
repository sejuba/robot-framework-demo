*** Settings ***
Documentation     A test suite with Gherkin style test.
...               This tests login functionality of an hypotectical web app
...               This test can be used as documentation for manual testers and also automation
Resource          ../resource/keywords/api_app_keywords.robot


*** Settings ***
Library	Collections
Library	RequestsLibrary

*** Test Cases ***
testing google :D
    check if google.com is live
    verify that google return status code 200

get full name of a given github user
    get the full name of user bulkan on github
    verify that full name is Bulkan Evcimen


