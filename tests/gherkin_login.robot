*** Settings ***
Documentation     A test suite with Gherkin style test.
...               This tests login functionality of an hypotectical web app
...               This test can be used as documentation for manual testers and also automation
Resource          ../resource/keywords/web_app_keywords.robot

Test Teardown     Close Browser

*** Test Cases ***
Valid Login
    Given browser is opened to login page
    When user "demo" logs in with password "mode"
    Then welcome page should be open

Invalid Username    
     Given browser is opened to login page
     When user "invalid " logs in with password "mode"    
     Then Login Should Have Failed 

Invalid Password   
     Given browser is opened to login page
     When user "invalid " logs in with password "mode"    
     Then Login Should Have Failed

Empty Username           
    Given browser is opened to login page
    When user "" logs in with password ""    
    Then Login Should Have Failed

Empty Password
    Given browser is opened to login page
    When user "" logs in with password ""    
    Then Login Should Have Failed

Empty Username And Password
    Given browser is opened to login page
    When user "" logs in with password ""
