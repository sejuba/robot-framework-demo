#! /bin/bash
echo ===installing robotframework and its dependencies ===
pip install -r requirement.txt  --user

echo === installing google chrome ===
wget https://chromedriver.storage.googleapis.com/2.35/chromedriver_linux64.zip &&
unzip chromedriver_linux64.zip &&
mv chromedriver /usr/bin/chromedriver &&
sudo chown root:root /usr/bin/chromedriver &&
sudo chmod +x /usr/bin/chromedriver &&
sudo -rf rm chromedriver_linux64.zip
