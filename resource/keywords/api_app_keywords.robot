*** Settings ***

Documentation   Importing web and robotframework built-in keywords.
Library	Collections
Library	RequestsLibrary

*** Keywords ***
#########################################################

check if google.com is live
    Create Session	google	http://www.google.com
    ${resp}=	Get Request	google	/
    set test variable  ${google http status}    ${resp}

verify that google return status code ${returned status code}
    Should Be Equal As Strings	${google http status.status_code}	${returned status code}

get the full name of user ${github id} on github
    Create Session	github	http://api.github.com
    ${resp}=	Get Request	github	/users/${github id}
    set test variable  ${returned githubinfo}    ${resp}

verify that full name is ${full name}
    Should Be Equal As Strings	${returned githubinfo.status_code}	200
    Dictionary Should Contain Value	${returned githubinfo.json()}	Bulkan Evcimen
    log to console  ${returned githubinfo.json()}

