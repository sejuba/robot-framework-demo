*** Settings ***

Documentation   Importing web and robotframework built-in keywords.
Library  Selenium2Library   timeout=10  #run_on_failure=CapturePageScreenShot

*** Variables ***
${SERVER}         localhost:7272
${BROWSER}        chrome
${DELAY}          0.5
${VALID USER}     demo
${VALID PASSWORD}    mode
${LOGIN URL}      http://${SERVER}/
${WELCOME URL}    http://${SERVER}/welcome.html
${ERROR URL}      http://${SERVER}/error.html

*** Keywords ***
#########################################################
browser is opened to login page
    [Documentation]
    [Tags]
    Open Browser    ${LOGIN URL}    ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}
    Login Page Should Be Open

Login Should Have Failed
    [Documentation]
    [Tags]
    Location Should Be    ${ERROR URL}
    Title Should Be    Error Page

Login Page Should Be Open
    [Documentation]
    [Tags]
    Title Should Be    Login Page

Go To Login Page
    [Documentation]
    [Tags]
    Go To    ${LOGIN URL}
    Login Page Should Be Open

Input Username
    [Documentation]
    [Tags]
    [Arguments]    ${username}
    Input Text    username_field    ${username}

Input Password
    [Documentation]
    [Tags]
    [Arguments]    ${password}
    Input Text    password_field    ${password}

Submit Credentials
    [Documentation]
    [Tags]
    Click Button    login_button

Welcome Page Should Be Open
    [Documentation]
    [Tags]
    Location Should Be    ${WELCOME URL}
    Title Should Be    Welcome Page

User "${username}" logs in with password "${password}"
    [Documentation]
    [Tags]
    Input username    ${username}
    Input password    ${password}
    Submit credentials

