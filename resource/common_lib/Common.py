import requests
import base64
import datetime
def get_time_export(strUrl, strAuth):

    url = strUrl
    headers = {
        'authorization': strAuth,
        'cache-control': "no-cache"
    }

    response = requests.request("GET", url, headers=headers)
    return (response.status_code)

def Remove_Whitespace(instring):
    return instring.strip()

def encode_string_to_base64(strEncoded):
    return base64.b64encode(strEncoded)
def get_current_day():
    now = datetime.datetime.now()
    return now.day


